import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:grow_more/models/plant_step.dart';
import 'package:grow_more/models/user.dart';
import 'package:grow_more/screens/home/wrapper.dart';
import 'package:grow_more/service_locator.dart';
import 'package:grow_more/utils/enum_plant_step.dart';
import 'package:grow_more/viewmodels/auth_viewmodel.dart';
import 'package:grow_more/viewmodels/home_viewmodel.dart';
import 'package:provider/provider.dart';
import 'package:workmanager/workmanager.dart';

import 'service_locator.dart';

void callbackDispatcher() async {
  Workmanager.executeTask((taskName, inputData) async {
    switch (taskName) {
      case 'firebaseTaskOnceOff':
      case 'firebaseTaskPeriodic':
        await Firestore.instance
            .collection("users")
            .getDocuments()
            .then((value) async {
          for (var u in value.documents) {
            User user = User.fromFireStore(u);
            await Firestore.instance
                .collection("users")
                .document(user.id)
                .collection("steps")
                .getDocuments()
                .then((value) async {
              for (var s in value.documents) {
                PlantStep plantStep = PlantStep.fromFireStore(s);
                if (DateTime.now().difference(plantStep.timeStamp).inHours >
                        23 &&
                    plantStep.plantStep != PlantSteps.STEP_7) {
                  PlantSteps updatedPlantStep;

                  switch (plantStep.plantStep) {
                    case PlantSteps.STEP_1:
                      updatedPlantStep = PlantSteps.STEP_2;
                      break;
                    case PlantSteps.STEP_2:
                      updatedPlantStep = PlantSteps.STEP_3;
                      break;
                    case PlantSteps.STEP_3:
                      updatedPlantStep = PlantSteps.STEP_4;
                      break;
                    case PlantSteps.STEP_4:
                      updatedPlantStep = PlantSteps.STEP_5;
                      break;
                    case PlantSteps.STEP_5:
                      updatedPlantStep = PlantSteps.STEP_6;
                      break;
                    case PlantSteps.STEP_6:
                      updatedPlantStep = PlantSteps.STEP_7;
                      break;
                    case PlantSteps.STEP_7:
                      updatedPlantStep = PlantSteps.STEP_7;
                      break;
                  }
                  await Firestore.instance
                      .collection("users")
                      .document(user.id)
                      .collection("steps")
                      .document(s.documentID)
                      .setData(
                          PlantStep.plantStepToFireStoreWithDate(
                            PlantStep(plantStep: updatedPlantStep),
                          ),
                          merge: true);
                  await Firestore.instance
                      .collection("users")
                      .document(user.id)
                      .get()
                      .then((value) async {
                    User us = User.fromFireStore(value);
                    await Firestore.instance
                        .collection("users")
                        .document(user.id)
                        .updateData({"score": us.score + 5});
                  });
                }
              }
            });
          }
        });
        break;
    }
    return Future.value(true);
  });
}

void main() {
  setupServiceLocator();
  runApp(MyApp());
  Workmanager.initialize(callbackDispatcher);
  Workmanager.registerOneOffTask("1111", "firebaseTaskOnceOff");
  Workmanager.registerPeriodicTask(
    "2222",
    "firebaseTaskPeriodic",
    frequency: Duration(hours: 6),
    constraints: Constraints(networkType: NetworkType.connected),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        StreamProvider.value(value: FirebaseAuth.instance.onAuthStateChanged),
        ChangeNotifierProvider(create: (context) => AuthViewModel()),
        ChangeNotifierProvider(create: (context) => HomeViewModel())
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Wrapper(),
      ),
    );
  }
}
