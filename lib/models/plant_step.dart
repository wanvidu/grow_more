import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/cupertino.dart';
import 'package:grow_more/utils/enum_plant_step.dart';
import 'package:json_annotation/json_annotation.dart';

part 'plant_step.g.dart';

@JsonSerializable()
class PlantStep{
	final PlantSteps plantStep;
	final DateTime timeStamp;

	PlantStep({@required this.plantStep, this.timeStamp});

	static Map<String, dynamic> plantStepToFireStore(PlantStep instance) =>
		<String, dynamic>{
			'step': EnumToString.parse(instance.plantStep),
			'created': FieldValue.serverTimestamp(),
		};

	static Map<String, dynamic> plantStepToFireStoreWithDate(PlantStep instance) =>
		<String, dynamic>{
			'step': EnumToString.parse(instance.plantStep),
			'created': Timestamp.now(),
		};

	factory PlantStep.fromFireStore(DocumentSnapshot doc) {

		return PlantStep(
			plantStep: EnumToString.fromString(PlantSteps.values,doc['step']),
			timeStamp: doc['created'].toDate(),
		);
	}

	factory PlantStep.fromJson(Map<String, dynamic> json) => _$PlantStepFromJson(json);

	Map<String, dynamic> toJson() => _$PlantStepToJson(this);
}