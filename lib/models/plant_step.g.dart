// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'plant_step.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PlantStep _$PlantStepFromJson(Map<String, dynamic> json) {
  return PlantStep(
    plantStep: _$enumDecodeNullable(_$PlantStepsEnumMap, json['plantStep']),
    timeStamp: json['timeStamp'] == null
        ? null
        : DateTime.parse(json['timeStamp'] as String),
  );
}

Map<String, dynamic> _$PlantStepToJson(PlantStep instance) => <String, dynamic>{
      'plantStep': _$PlantStepsEnumMap[instance.plantStep],
      'timeStamp': instance.timeStamp?.toIso8601String(),
    };

T _$enumDecode<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }

  final value = enumValues.entries
      .singleWhere((e) => e.value == source, orElse: () => null)
      ?.key;

  if (value == null && unknownValue == null) {
    throw ArgumentError('`$source` is not one of the supported values: '
        '${enumValues.values.join(', ')}');
  }
  return value ?? unknownValue;
}

T _$enumDecodeNullable<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<T>(enumValues, source, unknownValue: unknownValue);
}

const _$PlantStepsEnumMap = {
  PlantSteps.STEP_1: 'STEP_1',
  PlantSteps.STEP_2: 'STEP_2',
  PlantSteps.STEP_3: 'STEP_3',
  PlantSteps.STEP_4: 'STEP_4',
  PlantSteps.STEP_5: 'STEP_5',
  PlantSteps.STEP_6: 'STEP_6',
  PlantSteps.STEP_7: 'STEP_7',
};
