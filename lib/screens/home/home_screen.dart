import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:grow_more/screens/home/feed_screen.dart';
import 'package:grow_more/screens/home/plant_list.dart';
import 'package:grow_more/screens/notification.dart';
import 'package:grow_more/screens/profile.dart';
import 'package:grow_more/services/auth_service.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final AuthService _auth = AuthService();

  int pageIndex = 0;

  List<Widget> pageList = <Widget>[
    PlantList(),
    NotificationPage(),
    FeedScreen(),
    UserProfile(),
  ];

  final List<String> _titleList = [
    'My Garden',
    'Notifications',
    'Feed',
    'Profile',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_titleList[pageIndex]),
        backgroundColor: Colors.purple[900],
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.exit_to_app),
            onPressed: () async {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      title: Text("Logout"),
                      content: Text("Do you want to logout?"),
                      actions: <Widget>[
                        FlatButton(
                          child: Text("Yes"),
                          onPressed: () async {
                            Navigator.pop(context);
                            await _auth.signOut();
                          },
                        ),
                        FlatButton(
                          child: Text("No"),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                      ],
                    );
                  });
            },
          )
        ],
      ),
      body: PageTransitionSwitcher(
        transitionBuilder: (
          Widget child,
          Animation<double> animation,
          Animation<double> secondaryAnimation,
        ) {
          return FadeThroughTransition(
            animation: animation,
            secondaryAnimation: secondaryAnimation,
            child: child,
          );
        },
        child: pageList[pageIndex],
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: pageIndex,
        onTap: (int newValue) {
          setState(() {
            pageIndex = newValue;
          });
        },
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text("Home"),
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.notifications_active),
              title: Text("Notifications")),
          BottomNavigationBarItem(
            icon: Icon(Icons.image),
            title: Text("Feed"),
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.supervised_user_circle), title: Text("Profile")),
        ],
        selectedItemColor: Colors.purple[900],
        unselectedItemColor: Colors.purple,
        showUnselectedLabels: true,
      ),
    );
  }
}
