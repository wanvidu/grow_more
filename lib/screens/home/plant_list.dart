import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:grow_more/models/plant.dart';
import 'package:grow_more/screens/home/plant_details.dart';
import 'package:grow_more/screens/home/plant_progress.dart';
import 'package:grow_more/viewmodels/home_viewmodel.dart';
import 'package:provider/provider.dart';

class PlantList extends StatefulWidget {
  @override
  _PlantListState createState() => _PlantListState();
}

class _PlantListState extends State<PlantList> {
  @override
  Widget build(BuildContext context) {
    return Selector<HomeViewModel, List<Plant>>(
        selector: (context, model) => model.plantList,
        builder: (context, plants, child) {
          if (plants == null) {
            return Container(
              color: Colors.white,
              child: Center(
                child: SpinKitChasingDots(
                  color: Colors.purple,
                  size: 55.0,
                ),
              ),
            );
          } else if (plants.isEmpty) {
            return Container(
              color: Colors.purple[900],
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      Icons.info,
                      color: Colors.blue,
                      size: 60,
                    ),
                    const Padding(
                      padding: EdgeInsets.only(top: 16),
                      child: Text('No Plants in the List'),
                    )
                  ],
                ),
              ),
            );
          } else {
            return ListView.builder(
              itemCount: plants?.length,
              itemBuilder: (context, index) {
                return OpenContainer(
                  transitionType: ContainerTransitionType.fade,
                  closedBuilder: (BuildContext _, VoidCallback openContainer) {
                    return Padding(
                      padding: EdgeInsets.only(top: 10.0),
                      child: Container(
                        margin: new EdgeInsets.symmetric(
                            horizontal: 20.0, vertical: 10),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color: Color.fromRGBO(196, 135, 198, .3),
                              blurRadius: 10,
                              offset: Offset(0, 10),
                            )
                          ],
                        ),
                        child: Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20.0),
                          ),
                          child: Column(
                            children: <Widget>[
                              new ListTile(
                                leading: CircleAvatar(
                                  radius: 20,
                                  backgroundColor: Colors.green[50],
                                  backgroundImage: AssetImage(
                                      'assets/images/dashboardImages/plant.png'),
                                ),
                                title: new Text(plants[index].plantName),
                                subtitle: Text(
                                    'Lets grow ${plants[index].plantName} in your garden'),
                              ),
                              ButtonBar(
                                children: <Widget>[
                                  OpenContainer(
                                    transitionType:
                                        ContainerTransitionType.fade,
                                    closedBuilder: (BuildContext _,
                                        VoidCallback openContainer) {
                                      return Container(
                                        color: Colors.purple[500],
                                        padding: EdgeInsets.all(10),
                                        child: Text(
                                          "PROGRESS",
                                          style: TextStyle(color: Colors.white),
                                        ),
                                      );
                                    },
                                    openBuilder:
                                        (BuildContext context, VoidCallback _) {
                                      return PlantProgress(
                                        plant: plants[index],
                                      );
                                    },
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                  openBuilder: (BuildContext context, VoidCallback _) {
                    return PlantDetails(
                      plant: plants[index],
                    );
                  },
                );
              },
            );
          }
        });
  }
}
