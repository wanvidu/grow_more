import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:grow_more/models/plant.dart';
import 'package:grow_more/models/plant_step.dart';
import 'package:grow_more/utils/enum_plant_step.dart';
import 'package:grow_more/viewmodels/home_viewmodel.dart';
import 'package:provider/provider.dart';

class PlantProgress extends StatefulWidget {
  final Plant plant;

  PlantProgress({Key key, @required this.plant}) : super(key: key);

  @override
  _PlantProgressState createState() => _PlantProgressState();
}

class _PlantProgressState extends State<PlantProgress> {
  int _index = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Progress : ${widget.plant?.plantName}"),
        backgroundColor: Colors.purple[900],
      ),
      body: Scrollbar(
        child: SingleChildScrollView(
          child: FutureProvider(
            create: (BuildContext context) {
              return Provider.of<HomeViewModel>(context, listen: false)
                  .getPlantStep(widget.plant);
            },
            child: Consumer<PlantStep>(
              builder: (BuildContext context, PlantStep value, Widget child) {
                Widget widget;

                if (value != null) {
                  switch (value.plantStep) {
                    case PlantSteps.STEP_1:
                      _index = 0;
                      break;
                    case PlantSteps.STEP_2:
                      _index = 1;
                      break;
                    case PlantSteps.STEP_3:
                      _index = 2;
                      break;
                    case PlantSteps.STEP_4:
                      _index = 3;
                      break;
                    case PlantSteps.STEP_5:
                      _index = 4;
                      break;
                    case PlantSteps.STEP_6:
                      _index = 5;
                      break;
                    case PlantSteps.STEP_7:
                      _index = 6;
                      break;
                  }

                  widget = Stepper(
                    physics: ClampingScrollPhysics(),
                    steps: [
                      step_1(),
                      step_2(),
                      step_3(),
                      step_4(),
                      step_5(),
                      step_6(),
                      step_7(),
                    ],
                    currentStep: _index,
                    controlsBuilder: (BuildContext context,
                        {VoidCallback onStepContinue,
                        VoidCallback onStepCancel}) {
                      return Row(
                        children: <Widget>[
                          Container(
                            child: null,
                          ),
                          Container(
                            child: null,
                          ),
                        ],
                      );
                    },
                  );
                } else {
                  widget = Container(
                    margin: EdgeInsets.only(top: 50),
                    child: Center(
                      child: SpinKitChasingDots(
                        color: Colors.purple,
                        size: 55.0,
                      ),
                    ),
                  );
                }
                return widget;
              },
            ),
          ),
        ),
      ),
    );
  }
}

Step step_1() {
  return Step(
    title: Text(""),
    state: StepState.indexed,
    isActive: true,
    content: Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      elevation: 7,
      child: Container(
        margin: EdgeInsets.all(15),
        child: ListBody(
          mainAxis: Axis.vertical,
          children: <Widget>[
            Image.asset('assets/images/dashboardImages/garden.png'),
            Container(
              margin: EdgeInsets.only(top: 15),
              child: Text(
                "Plant is ready to plant",
                textAlign: TextAlign.center,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 15),
              child: Text(
                "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.",
                textAlign: TextAlign.justify,
                style: TextStyle(fontSize: 17),
              ),
            ),
          ],
        ),
      ),
    ),
  );
}

Step step_2() {
  return Step(
    title: Text(""),
    state: StepState.indexed,
    isActive: true,
    content: Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      elevation: 7,
      child: Container(
        margin: EdgeInsets.all(15),
        child: ListBody(
          mainAxis: Axis.vertical,
          children: <Widget>[
            Image.asset('assets/images/dashboardImages/garden.png'),
            Container(
              margin: EdgeInsets.only(top: 15),
              child: Text(
                "1st day passed",
                textAlign: TextAlign.center,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 15),
              child: Text(
                "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.",
                textAlign: TextAlign.justify,
                style: TextStyle(fontSize: 17),
              ),
            ),
          ],
        ),
      ),
    ),
  );
}

Step step_3() {
  return Step(
    title: Text(""),
    state: StepState.indexed,
    isActive: true,
    content: Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      elevation: 7,
      child: Container(
        margin: EdgeInsets.all(15),
        child: ListBody(
          mainAxis: Axis.vertical,
          children: <Widget>[
            Image.asset('assets/images/dashboardImages/garden.png'),
            Container(
              margin: EdgeInsets.only(top: 15),
              child: Text(
                "Plant is Growing",
                textAlign: TextAlign.center,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 15),
              child: Text(
                "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.",
                textAlign: TextAlign.justify,
                style: TextStyle(fontSize: 17),
              ),
            ),
          ],
        ),
      ),
    ),
  );
}

Step step_4() {
  return Step(
    title: Text(""),
    state: StepState.indexed,
    isActive: true,
    content: Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      elevation: 7,
      child: Container(
        margin: EdgeInsets.all(15),
        child: ListBody(
          mainAxis: Axis.vertical,
          children: <Widget>[
            Image.asset('assets/images/dashboardImages/garden.png'),
            Container(
              margin: EdgeInsets.only(top: 15),
              child: Text(
                "Great Job, plant is growing",
                textAlign: TextAlign.center,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 15),
              child: Text(
                "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.",
                textAlign: TextAlign.justify,
                style: TextStyle(fontSize: 17),
              ),
            ),
          ],
        ),
      ),
    ),
  );
}

Step step_5() {
  return Step(
    title: Text(""),
    state: StepState.indexed,
    isActive: true,
    content: Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      elevation: 7,
      child: Container(
        margin: EdgeInsets.all(15),
        child: ListBody(
          mainAxis: Axis.vertical,
          children: <Widget>[
            Image.asset('assets/images/dashboardImages/garden.png'),
            Container(
              margin: EdgeInsets.only(top: 15),
              child: Text(
                "Excellent, You are caring for the plants",
                textAlign: TextAlign.center,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 15),
              child: Text(
                "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.",
                textAlign: TextAlign.justify,
                style: TextStyle(fontSize: 17),
              ),
            ),
          ],
        ),
      ),
    ),
  );
}

Step step_6() {
  return Step(
    title: Text(""),
    state: StepState.indexed,
    isActive: true,
    content: Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      elevation: 7,
      child: Container(
        margin: EdgeInsets.all(15),
        child: ListBody(
          mainAxis: Axis.vertical,
          children: <Widget>[
            Image.asset('assets/images/dashboardImages/garden.png'),
            Container(
              margin: EdgeInsets.only(top: 15),
              child: Text(
                "You are close to complete",
                textAlign: TextAlign.center,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 15),
              child: Text(
                "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.",
                textAlign: TextAlign.justify,
                style: TextStyle(fontSize: 17),
              ),
            ),
          ],
        ),
      ),
    ),
  );
}

Step step_7() {
  return Step(
    title: Text(""),
    state: StepState.indexed,
    isActive: true,
    content: Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      elevation: 7,
      child: Container(
        margin: EdgeInsets.all(15),
        child: ListBody(
          mainAxis: Axis.vertical,
          children: <Widget>[
            Image.asset('assets/images/dashboardImages/garden.png'),
            Container(
              margin: EdgeInsets.only(top: 15),
              child: Text(
                "Plant has grown",
                textAlign: TextAlign.center,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 15),
              child: Text(
                "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.",
                textAlign: TextAlign.justify,
                style: TextStyle(fontSize: 17),
              ),
            ),
          ],
        ),
      ),
    ),
  );
}
